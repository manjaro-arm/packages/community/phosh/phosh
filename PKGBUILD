# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Dan Johansen
# Contributor: Jelle van der Waa <jelle@archlinux.org>
# Contributor: Thomas Booker <tw.booker@outlook.com>
# Contributor: Philip Goto <philip.goto@gmail.com>

pkgname=phosh
pkgver=0.45.0
pkgrel=0
pkgdesc="A pure Wayland shell for mobile devices"
arch=('x86_64' 'armv6h' 'armv7h' 'aarch64')
url="https://gitlab.gnome.org/World/Phosh/phosh"
license=('GPL-3.0-or-later')
depends=(
  'callaudiod'
  'evince'
  'evolution-data-server'
  'fribidi'
  'gcr'
  'gnome-bluetooth-3.0'
  'gnome-desktop'
  'gnome-session'
  'gnome-shell'
  'gtk3'
  'libadwaita'
  'libedataserverui4'
  'libgmobile'
  'libhandy'
  'libical'
  'libnm'
  'libpulse'
  'libsecret'
  'phoc'
  'polkit'
  'squeekboard'
  'upower'
  'wayland'
)
makedepends=(
  'feedbackd'
  'git'
  'glib2-devel'
  'meson'
  'python-docutils'
  'wayland-protocols'
)
checkdepends=(
  'xorg-xauth'
  'xorg-server-xvfb'
)
optdepends=(
  'feedbackd: haptic/visual/audio feedback'
  'iio-sensor-proxy: accelerometer and other sensors'
  'xdg-desktop-portal-gtk: for screenshot support'
  'xdg-desktop-portal-phosh: for accent colors in apps'
  'xdg-desktop-portal-wlr: for screencasts support'
)
install="$pkgname.install"
source=("git+https://gitlab.gnome.org/World/Phosh/phosh.git#tag=v$pkgver"
        '0001-system-prompt-allow-blank-passwords.patch'
        '0002-fix-locale-issue.patch'
        'https://gitlab.gnome.org/World/Phosh/phosh/-/merge_requests/977.patch'
        "pam_${pkgname}")
sha256sums=('9ea6f708084f1805cd060368ed808e8babb8379a67ee605119a8f09124ee326f'
            '0c5a2dbd0512ab8eca6e667f04ba03ec1b0d2896237b10d239aca63cfc19919e'
            '90c5c7cf519f2e162e1958d9a857e9f6c398206e26034d05cfb7991f540ea0ab'
            '2a910bdb4592ccb89f6bf93adf7d313428801c774115055eb92e1f7242ca1e0d'
            'b7793f80c533e84ad8adfe8bb46c69f107575e724aa9b53b41f370baa37e4fd5')

_reverts=(
)

prepare() {
  cd "$pkgname"

  for _c in "${_reverts[@]}"; do
    git log --oneline -1 "${_c}"
    git revert -n "${_c}"
  done

  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ "${src}" = *.patch ]] || continue
    echo "Applying patch ${src}..."
    patch -Np1 < "../${src}"
  done

  # Run meson once to setup the subproject for libcall-ui as it should not be packaged
  meson setup -D gtk_doc=false prep
}

build() {
  arch-meson "$pkgname" build \
    --libexecdir="/usr/lib/$pkgname" \
    -D tests=true \
    -D phoc_tests=disabled \
    -D man=true \
    -D gtk_doc=false \
    -D callui-i18n=false \
    -D lockscreen-plugins=true
  meson compile -C build
}

check() {
  xvfb-run meson test --no-suite screenshots -C build --no-rebuild --print-errorlogs || :

  desktop-file-validate build/data/mobi.phosh.Shell.desktop
}

package() {
  meson install -C build --no-rebuild --destdir "$pkgdir"

  # make squeekboard the default keyboard
  ln -s /usr/share/applications/sm.puri.Squeekboard.desktop \
    "${pkgdir}"/usr/share/applications/sm.puri.OSK0.desktop

  cd "${pkgname}"
  install -Dm644 "data/$pkgname.service" -t "$pkgdir"/usr/lib/systemd/system/
  install -Dm644 "$srcdir/pam_$pkgname" "$pkgdir/etc/pam.d/$pkgname"
}
